Small Soldiers PSX RSC file tools

Made in about half an hour on a whim when discussing Small Soldiers with a friend at 12:30am.

Includes library for loading RSC files, RSCInfoDumper for dumping filenamesd from RSC files and RSCExtractor for extracting files from RSC files.

I probably won't work on this further.

Download from https://gitlab.com/scrunguscrungus/smallsoldiersrsc/-/releases