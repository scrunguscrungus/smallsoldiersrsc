﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmallSoldiersRSCLib;

namespace RSCInfoDumper
{
	class Program
	{
		static void Main( string[] args )
		{
			Console.WriteLine( "RSC file:" );
			string infile = Console.ReadLine().Trim( '"' );
			Console.WriteLine( "Output file:" );
			string outfile = Console.ReadLine().Trim( '"' );

			if ( File.Exists( outfile ) )
			{
				Console.WriteLine( "File {0} exists, it will be overwritten.\nContinue? (Y/N)" );
				if ( Console.ReadLine().ToLower() != "y" || Console.ReadLine().ToLower() != "Y" )
					return;
			}

			Console.WriteLine( "Reading RSC..." );

			SmallSoldiersRSC RSC;
			try
			{
				RSC = SmallSoldiersRSC.Read( infile );
			}
			catch ( Exception e )
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine( "Failed to read RSC file!\n{0}", e.Message );
				Console.ReadKey();
				return;
			}

			using ( StreamWriter sw = new StreamWriter( File.Open( outfile, FileMode.Create ) ) )
			{
				foreach ( RSCEmbeddedFile file in RSC.Files )
				{
					sw.WriteLine( $"{file.Filename}" );
					sw.WriteLine();
				}
			}
		}
	}
}
