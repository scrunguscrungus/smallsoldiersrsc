﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SmallSoldiersRSCLib
{
	public class SmallSoldiersRSC
	{
		public string Filename;
		public List<RSCEmbeddedFile> Files;

		public static SmallSoldiersRSC Read( string filename )
		{
			if ( !File.Exists( filename ) )
				throw new FileNotFoundException($"File {filename} could not be found");

			SmallSoldiersRSC newRSC = new SmallSoldiersRSC();
			newRSC.Files = new List<RSCEmbeddedFile>();
			using ( BinaryReader br = new BinaryReader( File.Open( filename, FileMode.Open ) ) )
			{
				newRSC.Filename = ReadString( br );

				int FileCount = br.ReadInt32();
				_ = br.ReadInt32(); //Unknown

				for ( int i = 0; i<=FileCount-1; i++ )
				{
					RSCEmbeddedFile newFile = new RSCEmbeddedFile();

					newFile.Filename = ReadString( br );
					_ = br.ReadInt32(); //File index, do we need this? Is it important?
					int FileSize = br.ReadInt32();
					int FileOffset = br.ReadInt32();
					_ = br.ReadInt32(); //Unknown!

					long OldPos = br.BaseStream.Position;
					br.BaseStream.Seek( FileOffset, SeekOrigin.Begin );
					newFile.Data = br.ReadBytes( FileSize );
					br.BaseStream.Seek( OldPos, SeekOrigin.Begin );

					newRSC.Files.Add( newFile );
				}
			}
			return newRSC;
		}

		//Strings in RSC files are always allocated 64 bytes despite being null-terminated. This is a *very* lazy way of handling that.
		private static string ReadString( BinaryReader br )
		{
			byte[] stringBytes = br.ReadBytes( 64 );
			List<byte> realBytes = new List<byte>();

			foreach ( byte b in stringBytes )
			{
				if ( b != 0x0 )
					realBytes.Add( b );
				else
					break;
			}
			return Encoding.Default.GetString( realBytes.ToArray() );
		}
	}
}
