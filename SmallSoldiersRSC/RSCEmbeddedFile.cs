﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmallSoldiersRSCLib
{
	public struct RSCEmbeddedFile
	{
		public string Filename;
		public byte[] Data;
	}
}
