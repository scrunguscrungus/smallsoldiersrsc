﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using SmallSoldiersRSCLib;

namespace RSCExtractor
{
	class Program
	{
		static void Main( string[] args )
		{
			Console.WriteLine( "RSC file:" );
			string infile = Console.ReadLine().Trim('"');
			Console.WriteLine( "Output folder:" );
			string outfile = Console.ReadLine().Trim( '"' );

			//if ( !Directory.Exists( outfile ) )
			//{
			//	Directory.CreateDirectory( outfile );
			//}

			Console.WriteLine( "Reading RSC..." );

			SmallSoldiersRSC RSC;
			try
			{
				RSC = SmallSoldiersRSC.Read( infile );
			}
			catch ( Exception e )
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine( "Failed to read RSC file!\n{0}", e.Message );
				Console.ReadKey();
				return;
			}

			foreach ( RSCEmbeddedFile file in RSC.Files )
			{
				string outDir = Path.Combine( outfile, Path.GetDirectoryName( file.Filename ) );
				string extractFile = Path.Combine( outfile, file.Filename );

				if ( !Directory.Exists( outDir ) )
					Directory.CreateDirectory( outDir );

				using ( BinaryWriter bw = new BinaryWriter( File.Open( extractFile, FileMode.Create ) ) )
				{
					bw.Write( file.Data );
				}
			}
		}
	}
}
